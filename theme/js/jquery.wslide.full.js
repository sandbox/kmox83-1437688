/**
 * Based on wSlide 0.1 - http://www.webinventif.fr/wslide-plugin/
 *
 * Rendez vos sites glissant !
 *
 * Copyright (c) 2008 Julien Chauvin (webinventif.fr)
 * Licensed under the Creative Commons License:
 * http://creativecommons.org/licenses/by/3.0/
 *
 * Date: 2008-01-27
 */

/**
 * Function : dump()
 * Arguments: The data - array,hash(associative array),object
 *    The level - OPTIONAL
 * Returns  : The textual representation of the array.
 * This function was inspired by the print_r function of PHP.
 * This will accept some data as the argument and return a
 * text that will be a more readable version of the
 * array/hash/object that is given.
 * Docs: http://www.openjs.com/scripts/others/dump_function_php_print_r.php
 */

(function($) {
  $.fn.wslide = function(h) {

    /*
     * POSSIBLE PARAMETERS
     * width: the width of the div (Integer)
     * height: the height of the div (Integer)
     * pos: starting position in the slideshow (Integer)
     * col: Number of column (??) (Integer)
     * effect: The transition effect among slides (String)
     * fade: indicates if the fading transition is enabled (true,false)
     * horiz: Indicates if the scrolling is horizontal (true,false)
     * autolink: Indicates if the div for the links has to be created automatically
     * 			 true or false, or if a string is given it will correspond
     * 			 to the id of the div which will contain the links
     * duration: The duration of the fading transition
     * number_of_items: Number of items to skip for each transition
     * current: Has to be 1
     * disabled_class: the class to associate to the disabled net/prev button
     * enabled_class: the class to associate to the enabled net/prev button
     * total_link: Must be 0
     * wrap_around: indicates if when the end of the scroll is reached it has to start over from the opposite side
     * slide_forward: true->slides forward false->slides backward
     * on_click_stop_autoscroll: when user click on a link autoscroll is stopped
     * timeout: the time which has to pass during the autoscroll
     * */
    h = jQuery.extend({
      width : 150,
      height : 150,
      pos : 1,
      col : 1,
      effect : 'swing',
      fade : false,
      horiz : false,
      autoscroll : false,
      autolink : true,
      autolink_show : false,
      minipager : false,
      minipager_id : "wpager",
      duration : 1500,
      number_of_items : 4,
      current : 1,
      disabled_class : "wlast",
      enabled_class : "wmore",
      total_link : 0,
      wrap_around : false,
      slide_forward : true,
      on_click_stop_autoscroll : true,
      timeout : 3000

    }, h);

    function gogogo(g) {

      g.each(function(i) {

        var a = $(this);
        var uniqid = a.attr('id');

        h.minipager_id = uniqid + "_" + h.minipager_id;

        if(uniqid == undefined) {
          uniqid = 'wslide' + i;
        }
        console.dir(uniqid);
        $(this).wrap('<div class="wslide-wrap" id="' + uniqid + '-wrap"></div>');
        a = $('#' + uniqid + '-wrap');

        var b = a.find('ul li');
        var effets = h.effect;

        if(jQuery.easing.easeInQuad == undefined) {
          effets = 'swing';
        }

        var typex = h.width;
        var typey = h.height;
        function resultante(prop) {
          var tempcalc = prop;
          tempcalc = tempcalc.split('px');
          tempcalc = tempcalc[0];
          return Number(tempcalc);
        }

        var litypex = typex - (resultante(b.css('padding-left')) + resultante(b.css('padding-right')));
        var litypey = typey - (resultante(b.css('padding-top')) + resultante(b.css('padding-bottom')));
        var col = h.col;

        if(h.horiz) {
          col = Number(b.length + 1);
        }

        var manip = '';
        var ligne = Math.ceil(Number(b.length) / col);
        a.css('overflow', 'hidden').css('position', 'relative').css('text-align', 'left').css('height', typey + 'px').css('width', typex + 'px').css('margin', '0').css('padding', '0');
        a.find('ul').css('position', 'absolute').css('margin', '0').css('padding', '0').css('width', Number((col + 0) * typex) + 'px').css('height', Number(ligne * typey) + 'px');
        b.css('display', 'block').css('overflow', 'hidden').css('float', 'left').css('height', litypey + 'px').css('width', litypex + 'px');

        b.each(function(i) {
          // ADDING THE LINKS FOR THE PAGES
          //var offset = a.offset();
          //var thisoffset = $(this).offset();

          var v_offset = 0;
          var h_offset = 0;

          if(h.horiz) {
            h_offset = (i) * h.width;
          } else {
            v_offset = (i) * h.height;
          }

          //$(this).attr('id',uniqid+'-'+Number(i+1)).attr('rel', Number(thisoffset.left-offset.left)+':'+Number(thisoffset.top-offset.top));
          $(this).attr('id', uniqid + '-' + Number(i + 1)).attr('rel', h_offset + ':' + v_offset);
          manip += ' <a href="#' + uniqid + '-' + Number(i + 1) + '">' + Number(i + 1) + '</a>';
          h.total_link++;
        });
        if(h.autolink == 'true') {
          a.after('<div class="wslide-menu" id="' + uniqid + '-menu">' + manip + '</div>');

          // DO I HAVE TO SHOW AUTOLINK?
          if(!h.autolink_show) {
            var autolink_id = '#' + uniqid + '-menu';
            $(autolink_id).hide();
          }

        } else if(h.autolink == 'false') {
          // I DO NOTHING
        } else if( typeof h.autolink == 'string') {
          if($('#' + h.autolink).length) {
            $('#' + h.autolink).html(manip);
          } else {
            a.after('<div id="#' + h.autolink + '" style="">' + manip + '</div>');
          }

          // DO I HAVE TO SHOW AUTOLINK?
          if(!h.autolink_show) {
            var autolink_id = '#' + h.autolink;
            $(autolink_id).hide();
          }
        }

        var start = '#' + uniqid + '-';
        var stoccurent = "";

        $('a[href*="' + start + '"]').click(function() {

          // Remove from everyone the wactive class
          $('a[href*="' + '#' + uniqid + '"]').removeClass("wactive");

          // Add the wactive class to the clicked one
          $(this).addClass("wactive");

          var tri = $(this).attr('href');
          tri = tri.split('#');
          tri = '#' + tri[1];
          stoccurent = tri;

          h.current = tri.split('-');
          h.current = parseInt(h.current[1]);

          var decal = $(tri).attr('rel');
          decal = decal.split(':');

          var decal2 = decal[1];
          decal2 = -decal2;
          decal = decal[0];
          decal = -decal;

          if(h.fade) {
            a.find('ul').animate({
              opacity : 0
            }, h.duration / 2, effets, function() {
              $(this).css('top', decal2 + 'px').css('left', decal + 'px');
              $(this).animate({
                opacity : 1
              }, h.duration / 2, effets);
            });
          } else {
            /*var bcc = a.find('ul');
             bcc.each(function (){
             alert('animate({ top:'+ decal2+'px,left: '+decal+'px}');
             });*/
            a.find('ul').animate({
              top : decal2 + 'px',
              left : decal + 'px'
            }, h.duration, effets);
          }

          scroll_to_next();

          return false;
        });
console.dir(h.unique_id);
        $('a' + h.unique_id + '_next').click(function() {

          var next_click = (h.current + h.number_of_items);

          if(h.total_link < next_click) {
            next_click = h.total_link;
            if(h.wrap_around)
              next_click = 1;
          }

          if(h.total_link <= next_click) {
            // Add the wactive class to the clicked one
            if(!h.wrap_around) {
              // If I cannot wrap around I am on the last element
              $(this).removeClass(h.enabled_class);
              $(this).addClass(h.disabled_class);
              if(h.total_link > 1) {
                $('a' + h.unique_id + '_previous').addClass(h.enabled_class);
                $('a' + h.unique_id + '_previous').removeClass(h.disabled_class);
              }
            }
          } else {
            // I am not on the last element
            $(this).removeClass(h.disabled_class);
            $(this).addClass(h.enabled_class);

            //I may be on the first element
            if(next_click != 1) {
              $('a' + h.unique_id + '_previous').addClass(h.enabled_class);
              $('a' + h.unique_id + '_previous').removeClass(h.disabled_class);
            }
          }

          $('a[href$="' + start + next_click + '"]').click();

          return false;
        });

        $(h.unique_id + '_previous').click(function() {

          var next_click = (h.current - h.number_of_items);

          if(1 > next_click) {
            next_click = 1;
            if(h.wrap_around)
              next_click = h.total_link;
          }

          if(1 >= next_click) {
            if(!h.wrap_around) {
              $(this).removeClass(h.enabled_class);
              $(this).addClass(h.disabled_class);

              if(h.total_link != next_click) {
                $('a' + h.unique_id + '_next').addClass(h.enabled_class);
                $('a' + h.unique_id + '_next').removeClass(h.disabled_class);
              }
            }
          } else {
            $(this).removeClass(h.diabled_class);
            $(this).addClass(h.enabled_class);

            if(next_click != h.total_link) {
              $('a' + h.unique_id + '_next').addClass(h.enabled_class);
              $('a' + h.unique_id + '_next').removeClass(h.disabled_class);
            }
          }

          $('a[href="' + start + next_click + '"]').click();

          return false;
        });
        //Check if the autoscroll is active or not
        scroll_to_next();

        //Setting up the class for the next buttons
        if(h.wrap_around) {
          $('a' + h.unique_id + '_previous').addClass(h.enabled_class);
        } else {
          $('a' + h.unique_id + '_previous').addClass(h.disabled_class);
        }
        if((h.total_link > 1) || h.wrap_around) {
          $('a' + h.unique_id + '_next').addClass(h.enabled_class);
        } else {
          $('a' + h.unique_id + '_next').addClass(h.disabled_class);
        }

        if(h.pos <= 0) {
          h.pos = 1;
        }

        $('a[href$="' + start + h.pos + '"]').addClass("wactive");
        var tri = $('a[href*="' + start + '"]:eq(' + Number(h.pos - 1) + ')').attr('href');
        tri = tri.split('#');
        tri = '#' + tri[1];
        stoccurent = tri;
        var decal = $(tri).attr('rel');
        decal = decal.split(':');
        var decal2 = decal[1];
        decal2 = -decal2;
        decal = decal[0];
        decal = -decal;
        a.find('ul').css('top', decal2 + 'px').css('left', decal + 'px');

      });
      function scroll_to_next() {
        // CHECKING WHETER AUTOSCROLL IS ACTIVE OR NOT

        if(h.autoscroll) {
          if(h.slide_forward) {
            direction = '_next';
          } else {
            direction = '_previous';
          }
          var function_to_call = '$(\'a' + h.unique_id + direction + '\').click()';

          setTimeout(function_to_call, h.timeout);
        }

        /*** MINIPAGER SECTION ***/
        if(h.minipager) {
          var minipager_content = h.current + " of " + h.total_link;

          $('#' + h.minipager_id).html(minipager_content);
        }
      }

    }

    gogogo(this);
    return this;
  };
})(jQuery);
