$(document).ready(function(){

	//Drupal.behaviors.eai_main = function (context) {
	var wslide_settings = Drupal.settings['wslide'];
	console.dir(wslide_settings);
	$.each(wslide_settings, function(key, value) {
		var app = wslide_settings[key];
		var id_name = '#' + key;
		
		$(id_name).wslide({
			ajax: app.ajax,
			width: app.width,
			height: app.height,
			duration: app.duration,
			autoscroll:app.autoscroll,
			autolink:app.autolink,
			autolink_show:app.autolink_show,
			minipager:app.minipager,
			horiz:app.horiz,
			fade:app.fade,
			number_of_items:1, //app.number_of_items,
			autoscrolling:app.autoscrolling,
			disabled_class:app.disabled_class,
			unique_id:id_name,
			wrap_around:app.wrap_around,
			slide_forward:app.slide_forward,
			effect:app.effect,
			timeout:app.timeout
			
		});
	});

});
