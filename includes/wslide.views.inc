<?php

function wslide_views_plugins() {
  $plugins = array(
    'style' => array(
      'wslide' => array(
        'title' => t('Wslide view'),
        'help' => t('Wslide view jQuery plugin.'),
        'handler' => 'wslide_plugin_style_wslide',
        'theme' => 'wslide',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'parent' => 'list',
      ),
    )
   );
   
   return $plugins;
}
