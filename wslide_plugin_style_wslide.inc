<?php
/**
 * @file
 * Contains the wslide style plugin.
 */

/**
 * Style plugin to render the wslide.
 *
 * @ingroup views_style_plugins
 */
class wslide_plugin_style_wslide extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    
    $options['ajax'] = array('default' => false);
    $options['width'] = array('default' => '500');
    $options['height'] = array('default' => '150');
    $options['effect'] = array('default' => 'swing');
    $options['fade'] = array('default' => false);
    $options['horiz'] = array('default' => true);
    $options['autoscroll'] = array('default' => 'false');
    $options['autolink'] = array('default' => 'true');
    $options['autolink_show'] = array('default' => false);
    $options['number_of_items'] = array('default' => '6');
    $options['minipager'] = array('default' => 'false');
    $options['duration'] = array('default' => '1500');
    $options['disabled_class'] = array('default' => 'wlast');
    $options['enabled_class'] = array('default' => 'wmore');
    $options['wrap_around'] = array('default' => false);
    $options['slide_forward'] = array('default' => true);
    $options['timeout'] = array('default' => '3000');
	$options['wrapper_unique_id'] = array('default' => 'w'+ md5(uniqid()));
  
    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['wrapper_unique_id'] = array(
    '#title' => t('Wrapper Unique ID'),
          '#description' => t('Define a unique ID for the page where the view is placed.'),
          '#type' => 'textfield',
          '#size' => '30',
          '#default_value' => $this->options['wrapper_unique_id'],
        );
    
    $form['width'] = array(
      '#title' => t('Width'),
      '#description' => t('The width of the container.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['width'],
    );
    $form['height'] = array(
      '#title' => t('Height'),
      '#description' => t('The height of the container.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['height'],
    );	
    $form['effect'] = array(
      '#title' => t('Transition Effect'),
      '#description' => t('The effect applied on the transitions.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['effect'],
    );

    $form['fade'] = array(
      '#title' => t('Fade Enabled'),
      '#description' => t('The fading effect applied on the transitions.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['fade'],
    );
     
    $form['ajax'] = array(
      '#title' => t('Ajax Enabled'),
      '#description' => t('Ajax is enabled.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['ajax'],
    );
    $form['number_of_items'] = array(
      '#title' => t('Number of Items'),
      '#description' => t('The number of items to display.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['number_of_items'],
    );
    $form['horiz'] = array(
      '#title' => t('Horizontal'),
      '#description' => t('The direction of the slide flow.'),
      '#type' => 'checkbox',
      '#size' => '30',
      '#default_value' => $this->options['horiz'],
    );
    
    $form['autoscroll'] = array(
      '#title' => t('Autoscroll'),
      '#description' => t('The autoscroll function (true enabled, false disabled).'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['autoscroll'],
    );
    
    $form['autolink'] = array(
      '#title' => t('Autolink'),
      '#description' => t('The autolink function (true enabled, false disabled), you can put the id of the div where you want the pager to be placed.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['autolink'],
    );
    
    $form['autolink_show'] = array(
      '#title' => t('Show Autolink'),
      '#description' => t('Check this to show the autolink'),
      '#type' => 'checkbox',
      '#size' => '30',
      '#default_value' => $this->options['autolink_show'],
    );
    
    $form['minipager'] = array(
      '#title' => t('Show Minipager'),
      '#description' => t('Check this to show the minipager'),
      '#type' => 'checkbox',
      '#size' => '30',
      '#default_value' => $this->options['minipager'],
    );
    
    $form['duration'] = array(
      '#title' => t('Transition Effect Duration'),
      '#description' => t('Insert the duration of the transition effect in milliseconds.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['duration'],
    );
    
    $form['disabled_class'] = array(
      '#title' => t('Disabled Class'),
      '#description' => t('Class for the disabled items.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['disabled_class'],
    );
    
    $form['enabled_class'] = array(
      '#title' => t('Enabled Class'),
      '#description' => t('Class for the enabled items.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['enabled_class'],
    );
    
    $form['wrap_around'] = array(
      '#title' => t('Wrap Around'),
      '#description' => t('Toggle the infinite scrolling/wrap around.'),
      '#type' => 'checkbox',
      '#size' => '30',
      '#default_value' => $this->options['wrap_around'],
    );
    
    $form['slide_forward'] = array(
      '#title' => t('Slide Forward'),
      '#description' => t('If checked the slides move forward, otherwise it moves backward.'),
      '#type' => 'checkbox',
      '#size' => '30',
      '#default_value' => $this->options['slide_forward'],
    );
    
    $form['timeout'] = array(
      '#title' => t('Timeout'),
      '#description' => t('The timeout for the autoscrolling in milliseconds.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['timeout'],
    );
    
  }
}
  
  
  /**
   * Set default options
   */
  /*function option_definition() {
    $options = parent::option_definition();
    
    return $options;
  }   */

  /**
   * Render the given style.
   */
/*
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
}   */
